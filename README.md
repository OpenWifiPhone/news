Articles sur la téléphonie
==========================

* [Quel système d'exploitation pour nos téléphones ?](systemes_d-exploitation_plus_ou_moins_libres_pour_telephones.md)

Référence
---------

Le code Markdown de l'article se trouve sur deux dépôts Git :

* https://framagit.org/OpenWifiPhone/news
* https://github.com/OpenWifiPhone/news


Licence
-------

CC BY-SA 3.0  https://creativecommons.org/licenses/by-sa/3.0/deed.fr  
Le code légal est fournit dans le fichier [LICENSE](LICENSE).

Le texte contenu dans les fichiers de ce dépôt peut être copié vers Wikipédia
avec comme restriction l'indication des auteurs originaires.  
Merci aussi de mentioner un lien vers ce dépôt.
